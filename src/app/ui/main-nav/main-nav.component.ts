import { Component, OnInit , Input } from '@angular/core';
import { AuthService } from './../../core/auth.service';
import { Observable } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';
import { NotifyService } from './../../core/notify.service';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
@Component({
  selector: 'main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent {
  show = false;
  @Input() IsLoggedIn: Boolean;
  constructor(private auth: AuthService, private notify: NotifyService, private router: Router  ) { }

  toggleCollapse() {
    this.show = !this.show;
  }
  logout() {
    this.auth.signOut();
  }

}
