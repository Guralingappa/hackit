import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
 items: Array<any> = new Array<any>();
  constructor() { }

  ngOnInit() {
    this.items.push({'PatientId': '1', 'TrialName': 'Trail check'});
  }

}
