export const environment = {
  production: true,
  firebase: {
    apiKey: 'Test',
        authDomain: 'test',
    databaseURL: 'test',
    projectId: 'test',
    storageBucket: 'test',
    messagingSenderId: 'test'
  }
};
